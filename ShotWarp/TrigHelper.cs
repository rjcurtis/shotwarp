﻿using System;
using SFML.Window;
using NetEXT.MathFunctions;

namespace ShotWarp
{
    public static class TrigHelper
    {
        public static float Distance(Vector2f A, Vector2f B)
        {
            return (float)Trigonometry.SqrRoot((B.X - A.X) * (B.X - A.X) + (B.Y - A.Y) * (B.Y - A.Y));
        }
        public static float Angle(Vector2f A, Vector2f B)
        {
            return (float)Trigonometry.ArcTan2(B.Y - A.Y, B.X - A.X);
        }
    }
}
