﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.ScreenManager;
using Spitfire.Resources;
using NetEXT.TimeFunctions;
using ShotWarp.ScreenManagers.GameScreen_Controls;

namespace ShotWarp.ScreenManagers
{
    public class FinishedScreen : ScreenManagerUIBase
    {
        #region Variables
        private Time _runningtime = Time.Zero;
        private int _totalshots = 0;
        private int _hitshots = 0;
        private int _missedshots = 0;
        #endregion

        #region Constructors
        public FinishedScreen(Vector2f CurrentScreenSize, ResourceManager CurrentResourceManager, Time RunningTime, int TotalShots, int HitShots, int MissedShots)
            : base(CurrentScreenSize, CurrentResourceManager)
        {
            _runningtime = RunningTime;
            _totalshots = TotalShots;
            _hitshots = HitShots;
            _missedshots = MissedShots;
        }
        #endregion

        #region Functions
        public override void ScreenActivated()
        {
            MusicManager.Pause();
            base.ScreenActivated();
        }
        public override void ScreenDeactivated()
        {
            MusicManager.Play();
            base.ScreenDeactivated();
        }
        protected override void BuildUI()
        {
            AddControl(new TextDisplay(ResourceManager) { FontSize = 35, Color = Color.White, Text = "Congratulations - Game Complete :D", Position = new Vector2f(25, 200) });
            AddControl(new TextDisplay(ResourceManager) { FontSize = 25, Color = Color.White, Text = "(Press 'Enter')", Position = new Vector2f(280, 250) });

            AddControl(new TextDisplay(ResourceManager) { FontSize = 25, Color = Color.White, Text = "Time: " + Math.Round(_runningtime.Seconds, 2) + " seconds", Position = new Vector2f(240, 360) });
            AddControl(new TextDisplay(ResourceManager) { FontSize = 25, Color = Color.White, Text = "Total Shots: " + _totalshots, Position = new Vector2f(240, 390) });
            AddControl(new TextDisplay(ResourceManager) { FontSize = 25, Color = Color.White, Text = "Hit Shots: " + _hitshots, Position = new Vector2f(240, 420) });
            AddControl(new TextDisplay(ResourceManager) { FontSize = 25, Color = Color.White, Text = "Missed Shots: " + _missedshots, Position = new Vector2f(240, 450) });

            AddControl(new TextDisplay(ResourceManager) { FontSize = 15, Color = Color.White, Text = "Written by Zachariah Brown", Position = new Vector2f(450, 580) });
        }
        public override void KeyPressed(RenderTarget Target, KeyEventArgs EventArgs)
        {
            if (EventArgs.Code == Keyboard.Key.Return) OnCloseScreen();
            base.KeyPressed(Target, EventArgs);
        }
        #endregion
    }
}
