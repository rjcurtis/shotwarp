﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.ScreenManager;
using Spitfire.Resources;
using NetEXT.TimeFunctions;
using ShotWarp.ScreenManagers.GameScreen_Controls;

namespace ShotWarp.ScreenManagers
{
    public class GameScreen : ScreenManagerUIBase
    {
        #region Variables
        private Launch _launchcontrol = null;
        private Arrows _arrowscontrol = null;
        private Targets _targetscontrol = null;
        private GravityState _gravitystate = GravityState.Split;
        private Gravity _leftgravity = null;
        private Gravity _rightgravity = null;
        private Gravity _spacegravity = null;
        private GravityDrawing _gravitycontrol = null;
        private FloatRect _walls = new FloatRect(0, 0, 700, 600);
        private FloatRect _spacearea = new FloatRect(150, 0, 400, 300);
        private Warp _leftwarp = null;
        private Warp _rightwarp = null;
        private Warp _spaceleft = null;
        private Warp _spaceright = null;
        private LevelLoader _loader = new LevelLoader();
        private Level _level = null;
        private int _lefthit = 0;
        private int _righthit = 0;
        private int _spacehit = 0;
        private int _ammoremaining = 0;
        private TextDisplay _lefthitdisplay = null;
        private TextDisplay _righthitdisplay = null;
        private TextDisplay _spacehitdisplay = null;
        private TextDisplay _ammodisplay = null;
        private Timer _endtimer = new Timer();
        private GameState _state = GameState.GameRunning;
        private Time _runningtime = Time.Zero;
        private int _totalshots = 0;
        private int _hitshots = 0;
        private int _missedshots = 0;
        #endregion

        #region Constructors
        public GameScreen(Vector2f CurrentScreenSize, ResourceManager CurrentResourceManager)
            : base(CurrentScreenSize, CurrentResourceManager) { }
        #endregion

        #region Functions
        public override void ScreenActivated()
        {
            if (_state == GameState.GameFinished) OnCloseScreen();
            else base.ScreenActivated();
        }
        protected override void BuildUI()
        {
            AddControl(new Background(ResourceManager) { ZOrder = -1 });
            _gravitycontrol = new GravityDrawing(ResourceManager);
            AddControl(_gravitycontrol);
            _rightwarp = new Warp(ResourceManager, -123.6f) { Position = new Vector2f(550, 600) };
            AddControl(_rightwarp);
            _leftwarp = new Warp(ResourceManager, -56.3f) { Position = new Vector2f(150, 600) };
            AddControl(_leftwarp);
            _spaceright = new Warp(ResourceManager, 123.6f) { Position = new Vector2f(554.43f, -6.66f) };
            AddControl(_spaceright);
            _spaceleft = new Warp(ResourceManager, 56.3f) { Position = new Vector2f(145.56f, -6.66f) };
            AddControl(_spaceleft);
            _launchcontrol = new Launch(ResourceManager) { ZOrder = 1 };
            _launchcontrol.Fire += Fire;
            AddControl(_launchcontrol);
            _targetscontrol = new Targets(ResourceManager) { ZOrder = 1 };
            AddControl(_targetscontrol);
            _lefthitdisplay = new TextDisplay(ResourceManager) { Color = Color.Black, Position = new Vector2f(50, 5), FontSize = 25, ZOrder = 5 };
            AddControl(_lefthitdisplay);
            _righthitdisplay = new TextDisplay(ResourceManager) { Color = Color.Black, Position = new Vector2f(600, 5), FontSize = 25, ZOrder = 5 };
            AddControl(_righthitdisplay);
            _spacehitdisplay = new TextDisplay(ResourceManager) { Color = Color.Red, Position = new Vector2f(315, 5), FontSize = 25, ZOrder = 5 };
            AddControl(_spacehitdisplay);
            _ammodisplay = new TextDisplay(ResourceManager) { Color = Color.Black, Position = new Vector2f(275, 550), FontSize = 25, ZOrder = 5 };
            AddControl(_ammodisplay);
            _arrowscontrol = new Arrows(ResourceManager) { ZOrder = 6 };
            AddControl(_arrowscontrol);
            AddControl(new Globe(ResourceManager) { Position = new Vector2f(317.5f, 267.5f), ZOrder = 10 });
            AddControl(new Ground(ResourceManager) { Position = new Vector2f(0, 592), ZOrder = 10 });
            _level = _loader.LoadCurrentLevel();
            BindLevelEvents();
            _level.Initialize();
            ResetLevelState();
        }
        private void Fire(float VelocityScale, float Angle, Vector2f Position)
        {
            Arrow newarrow = new Arrow(VelocityScale, Angle, Position);
            newarrow.Dead += DeadArrow;
            _arrowscontrol.AddArrow(newarrow);
            _totalshots += 1;
            if (_level.TotalAmmo > 0)
            {
                _ammoremaining -= 1;
                if (_ammoremaining == 0) _launchcontrol.CanFire = false;
            }
            AudioManager.Play(@".\\Sound\arrow shoot.ogg");
        }
        private void DeadArrow(Arrow Arrow)
        {
            _arrowscontrol.RemoveArrow(Arrow);
            Arrow.Dead -= DeadArrow;
        }
        private void DeadTarget(Target Target)
        {
            _targetscontrol.RemoveTarget(Target);
            Target.Dead -= DeadTarget;
        }
        public override void Update(Time DeltaTime)
        {
            _runningtime += DeltaTime;
            foreach (var arrow in _arrowscontrol.ArrowsList)
            {
                if (_gravitystate == GravityState.Split)
                {
                    if (arrow.State == ArrowState.FlyingLeft)
                    {
                        arrow.Update(DeltaTime, _leftgravity);
                    }
                    else if (arrow.State == ArrowState.FlyingRight)
                    {
                        arrow.Update(DeltaTime, _rightgravity);
                    }
                    else
                    {
                        arrow.Update(DeltaTime, null);
                    }
                }
                else if (_gravitystate == GravityState.Space)
                {
                    if (arrow.State == ArrowState.FlyingLeft || arrow.State == ArrowState.FlyingRight || arrow.State == ArrowState.FlyingSpace)
                    {
                        arrow.Update(DeltaTime, _spacegravity);
                    }
                    else
                    {
                        arrow.Update(DeltaTime, null);
                    }
                }
            }
            foreach (var target in _targetscontrol.TargetList)
            {
                target.Update(DeltaTime);
            }
            foreach (var arrow in _arrowscontrol.ArrowsList)
            {
                if (arrow.State == ArrowState.Launched)
                {
                    if (!_launchcontrol.Contains(arrow.Position))
                    {
                        if (arrow.Position.Y >= _launchcontrol.Position.Y + _launchcontrol.Size.Y)
                        {
                            arrow.State = ArrowState.Dead;
                            AudioManager.Play(@".\\Sound\arrow died.ogg");
                            _missedshots += 1;
                        }
                        else if (TrigHelper.Distance(arrow.Position, new Vector2f(350, 300)) <= 35f)
                        {
                            arrow.State = ArrowState.Dead;
                            AudioManager.Play(@".\\Sound\arrow died.ogg");
                            _missedshots += 1;
                        }
                        else if (arrow.Position.X <= 350f)
                        {
                            arrow.State = ArrowState.SuspendedLeft;
                            arrow.SuspendTime = _leftwarp.WarpTime;
                            AudioManager.Play(@".\\Sound\warp enter.ogg");
                        }
                        else if (arrow.Position.X >= 350f)
                        {
                            arrow.State = ArrowState.SuspendedRight;
                            arrow.SuspendTime = _rightwarp.WarpTime;
                            AudioManager.Play(@".\\Sound\warp enter.ogg");
                        }
                    }
                }
                else if (arrow.State == ArrowState.FlyingLeft)
                {
                    if (TrigHelper.Distance(arrow.Position, new Vector2f(350, 300)) <= 25f)
                    {
                        arrow.State = ArrowState.Dead;
                        AudioManager.Play(@".\\Sound\arrow died.ogg");
                        _missedshots += 1;
                    }
                    else if (!_walls.Contains(arrow.Position.X, arrow.Position.Y))
                    {
                        arrow.State = ArrowState.Dead;
                        AudioManager.Play(@".\\Sound\arrow died.ogg");
                        _missedshots += 1;
                    }
                    else
                    {
                        foreach (var target in _targetscontrol.TargetList)
                        {
                            if (target.Type == TargetType.Left && target.State == TargetState.Alive)
                            {
                                if (target.Hit(arrow))
                                {
                                    _lefthit += 1;
                                    _hitshots += 1;
                                    arrow.State = ArrowState.Dead;
                                    target.StickArrow(arrow);
                                    AudioManager.Play(@".\\Sound\arrow hit.ogg");
                                }
                            }
                        }
                        if (_spacearea.Contains(arrow.Position.X, arrow.Position.Y))
                        {
                            if (TrigHelper.Angle(_spaceleft.Position, arrow.Position) < _spaceleft.Rotation)
                            {
                                arrow.State = ArrowState.SuspendedSpace;
                                arrow.SuspendTime = _spaceleft.WarpTime;
                                AudioManager.Play(@".\\Sound\warp enter.ogg");
                            }
                        }
                    }
                }
                else if (arrow.State == ArrowState.FlyingRight)
                {
                    if (TrigHelper.Distance(arrow.Position, new Vector2f(350, 300)) <= 25f)
                    {
                        arrow.State = ArrowState.Dead;
                        AudioManager.Play(@".\\Sound\arrow died.ogg");
                        _missedshots += 1;
                    }
                    else if (!_walls.Contains(arrow.Position.X, arrow.Position.Y))
                    {
                        arrow.State = ArrowState.Dead;
                        AudioManager.Play(@".\\Sound\arrow died.ogg");
                        _missedshots += 1;
                    }
                    else
                    {
                        foreach (var target in _targetscontrol.TargetList)
                        {
                            if (target.Type == TargetType.Right && target.State == TargetState.Alive)
                            {
                                if (target.Hit(arrow))
                                {
                                    _righthit += 1;
                                    _hitshots += 1;
                                    arrow.State = ArrowState.Dead;
                                    target.StickArrow(arrow);
                                    AudioManager.Play(@".\\Sound\arrow hit.ogg");
                                }
                            }
                        }
                        if (_spacearea.Contains(arrow.Position.X, arrow.Position.Y))
                        {
                            if (TrigHelper.Angle(_spaceright.Position, arrow.Position) > _spaceright.Rotation)
                            {
                                arrow.State = ArrowState.SuspendedSpace;
                                arrow.SuspendTime = _spaceright.WarpTime;
                                AudioManager.Play(@".\\Sound\warp enter.ogg");
                            }
                        }
                    }
                }
                else if (arrow.State == ArrowState.FlyingSpace)
                {
                    if (TrigHelper.Distance(arrow.Position, new Vector2f(350, 300)) <= 25f)
                    {
                        arrow.State = ArrowState.Dead;
                        AudioManager.Play(@".\\Sound\arrow died.ogg");
                        _missedshots += 1;
                    }
                    else if (!_walls.Contains(arrow.Position.X, arrow.Position.Y))
                    {
                        arrow.State = ArrowState.Dead;
                        AudioManager.Play(@".\\Sound\arrow died.ogg");
                        _missedshots += 1;
                    }
                    else
                    {
                        bool collided = false;
                        if (arrow.Position.X <= 350)
                        {
                            if (TrigHelper.Angle(_spaceleft.Position, arrow.Position) > _spaceleft.Rotation)
                            {
                                arrow.State = ArrowState.Dead;
                                _missedshots += 1;
                                AudioManager.Play(@".\\Sound\arrow died.ogg");
                                collided = true;
                            }
                        }
                        else
                        {
                            if (TrigHelper.Angle(_spaceright.Position, arrow.Position) < _spaceright.Rotation)
                            {
                                arrow.State = ArrowState.Dead;
                                _missedshots += 1;
                                AudioManager.Play(@".\\Sound\arrow died.ogg");
                                collided = true;
                            }
                        }
                        if (!collided)
                        {
                            foreach (var target in _targetscontrol.TargetList)
                            {
                                if (target.Type == TargetType.Space && target.State == TargetState.Alive)
                                {
                                    if (target.Hit(arrow))
                                    {
                                        _spacehit += 1;
                                        _hitshots += 1;
                                        arrow.State = ArrowState.Dead;
                                        target.StickArrow(arrow);
                                        AudioManager.Play(@".\\Sound\arrow hit.ogg");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (_level.RequiredLeftTargets > 0) _lefthitdisplay.Text = _lefthit + " / " + _level.RequiredLeftTargets;
            else _lefthitdisplay.Text = "";
            if (_level.RequiredRightTargets > 0) _righthitdisplay.Text = _righthit + " / " + _level.RequiredRightTargets;
            else _righthitdisplay.Text = "";
            if (_level.RequiredSpaceTargets > 0) _spacehitdisplay.Text = _spacehit + " / " + _level.RequiredSpaceTargets;
            else _spacehitdisplay.Text = "";
            if (_level.TotalAmmo > 0) _ammodisplay.Text = _ammoremaining + " / " + _level.TotalAmmo + " Arrows";
            else _ammodisplay.Text = "";
            _level.Update(DeltaTime);
            CheckGameState();
            base.Update(DeltaTime);
        }
        private void CheckGameState()
        {
            if (_state == GameState.GameRunning)
            {
                if (_lefthit >= _level.RequiredLeftTargets && _righthit >= _level.RequiredRightTargets && _spacehit >= _level.RequiredSpaceTargets)
                {
                    _state = GameState.WaitingToFinishLevel;
                    _endtimer.Restart(Time.FromSeconds(5));
                    AudioManager.Play(@".\\Sound\quota.ogg");
                }
                else if (_level.TotalAmmo > 0 && _ammoremaining == 0 && _arrowscontrol.ArrowsList.Length == 0)
                {
                    _state = GameState.WaitingToFailLevel;
                    _endtimer.Restart(Time.FromSeconds(3));
                }
            }
            else if (_state == GameState.WaitingToFailLevel)
            {
                if (_endtimer.IsExpired)
                {
                    UnbindLevelEvents();
                    ClearLevelState();
                    _level = _loader.LoadCurrentLevel();
                    BindLevelEvents();
                    _level.Initialize();
                    ResetLevelState();
                    AudioManager.Play(@".\\Sound\level fail.ogg");
                    OnSwitchScreen(new FailedScreen(ScreenSize, ResourceManager));
                }
            }
            else if (_state == GameState.WaitingToFinishLevel)
            {
                if (_endtimer.IsExpired)
                {
                    if (!_loader.IsFinalLevel)
                    {
                        UnbindLevelEvents();
                        ClearLevelState();
                        _level = _loader.LoadNextLevel();
                        BindLevelEvents();
                        _level.Initialize();
                        ResetLevelState();
                        AudioManager.Play(@".\\Sound\level complete.ogg");
                        OnSwitchScreen(new CompletedScreen(ScreenSize, ResourceManager));
                    }
                    else
                    {
                        UnbindLevelEvents();
                        ClearLevelState();
                        _state = GameState.GameFinished;
                        AudioManager.Play(@".\\Sound\game finish.ogg");
                        OnSwitchScreen(new FinishedScreen(ScreenSize, ResourceManager, _runningtime, _totalshots, _hitshots, _missedshots));
                    }
                }
            }
        }
        private void ClearLevelState()
        {
            foreach (var arrow in _arrowscontrol.ArrowsList)
            {
                DeadArrow(arrow);
            }
            foreach (var target in _targetscontrol.TargetList)
            {
                DeadTarget(target);
            }
        }
        private void ResetLevelState()
        {
            _ammoremaining = _level.TotalAmmo;
            _launchcontrol.CanFire = true;
            _lefthit = 0;
            _righthit = 0;
            _spacehit = 0;
            _state = GameState.GameRunning;
        }
        private void BindLevelEvents()
        {
            _level.AddTarget += LevelAddTarget;
            _level.SetGravityState += LevelSetGravityState;
            _level.SetLeftGravity += LevelSetLeftGravity;
            _level.SetRightGravity += LevelSetRightGravity;
            _level.SetSpaceGravity += LevelSetSpaceGravity;
            _level.SetLeftWarpTime += LevelSetLeftWarpTime;
            _level.SetRightWarpTime += LevelSetRightWarpTime;
            _level.SetSpaceWarpTime += LevelSetSpaceWarpTime;
        }
        private void UnbindLevelEvents()
        {
            _level.AddTarget -= LevelAddTarget;
            _level.SetGravityState -= LevelSetGravityState;
            _level.SetLeftGravity -= LevelSetLeftGravity;
            _level.SetRightGravity -= LevelSetRightGravity;
            _level.SetSpaceGravity -= LevelSetSpaceGravity;
            _level.SetLeftWarpTime -= LevelSetLeftWarpTime;
            _level.SetRightWarpTime -= LevelSetRightWarpTime;
            _level.SetSpaceWarpTime -= LevelSetSpaceWarpTime;
        }
        void LevelSetSpaceGravity(Gravity Gravity)
        {
            if (_spacegravity != null && _gravitystate == GravityState.Space) _gravitycontrol.RemoveGravity(_spacegravity);
            _spacegravity = Gravity;
            if (_spacegravity != null && _gravitystate == GravityState.Space) _gravitycontrol.AddGravity(_spacegravity);
        }
        void LevelSetRightGravity(Gravity Gravity)
        {
            if (_rightgravity != null && _gravitystate == GravityState.Split) _gravitycontrol.RemoveGravity(_rightgravity);
            _rightgravity = Gravity;
            if (_rightgravity != null && _gravitystate == GravityState.Split) _gravitycontrol.AddGravity(_rightgravity);
        }
        void LevelSetLeftGravity(Gravity Gravity)
        {
            if (_leftgravity != null && _gravitystate == GravityState.Split) _gravitycontrol.RemoveGravity(_leftgravity);
            _leftgravity = Gravity;
            if (_leftgravity != null && _gravitystate == GravityState.Split) _gravitycontrol.AddGravity(_leftgravity);
        }
        void LevelSetGravityState(GravityState GravityState)
        {
            if (_leftgravity != null) _gravitycontrol.RemoveGravity(_leftgravity);
            if (_rightgravity != null) _gravitycontrol.RemoveGravity(_rightgravity);
            if (_spacegravity != null) _gravitycontrol.RemoveGravity(_spacegravity);
            _gravitystate = GravityState;
            if (_gravitystate == ShotWarp.GravityState.Split)
            {
                if (_leftgravity != null) _gravitycontrol.AddGravity(_leftgravity);
                if (_rightgravity != null) _gravitycontrol.AddGravity(_rightgravity);
            }
            else if (_gravitystate == ShotWarp.GravityState.Space)
            {
                if (_spacegravity != null) _gravitycontrol.AddGravity(_spacegravity);
            }
        }
        void LevelAddTarget(Target Target)
        {
            Target.Dead += DeadTarget;
            _targetscontrol.AddTarget(Target);
        }
        void LevelSetLeftWarpTime(Time Time)
        {
            _leftwarp.WarpTime = Time;
        }
        void LevelSetRightWarpTime(Time Time)
        {
            _rightwarp.WarpTime = Time;
        }
        void LevelSetSpaceWarpTime(Time Time)
        {
            _spaceleft.WarpTime = Time;
            _spaceright.WarpTime = Time;
        }
        public override void KeyPressed(RenderTarget Target, KeyEventArgs EventArgs)
        {
            if (EventArgs.Code == Keyboard.Key.Escape) OnSwitchScreen(new PauseScreen(ScreenSize, ResourceManager));
            base.KeyPressed(Target, EventArgs);
        }
        #endregion
    }
}
