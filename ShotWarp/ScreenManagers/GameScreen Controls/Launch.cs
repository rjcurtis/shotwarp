﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.Resources;
using NetEXT.MathFunctions;

namespace ShotWarp.ScreenManagers.GameScreen_Controls
{
    public class Launch : ControlBase
    {
        #region Variables
        private Triangle _trianglecollision = new Triangle(new Vector2f(350, 323), new Vector2f(165, 600), new Vector2f(535, 600));
        private bool _mousedown = false;
        private Vector2f _downpoint = new Vector2f();
        private RectangleShape _aimline = new RectangleShape(new Vector2f(0, 15)) { Origin = new Vector2f(0, 7.5f) };
        private bool _canfire = true;
        #endregion

        #region Properties
        public bool CanFire
        {
            get
            {
                return _canfire;
            }
            set
            {
                _canfire = value;
                if (!_canfire && _mousedown) _mousedown = false;
            }
        }
        #endregion

        #region Events
        public event Action<float, float, Vector2f> Fire;
        #endregion

        #region Constructors
        public Launch(ResourceManager Manager)
        {
            Size = new Vector2f(700, 600);
            _aimline.Texture = Manager.GetTexture(@".\\Game\Aim.png");
        }
        #endregion

        #region Functions
        public override void MouseButtonPressed(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            if (_canfire)
            {
                Vector2f mousepos = Target.MapPixelToCoords(new Vector2i(EventArgs.X, EventArgs.Y));
                mousepos = GetMouseRelativePosition(mousepos);
                if (_trianglecollision.ContainsPoint(mousepos.X, mousepos.Y) && EventArgs.Button == Mouse.Button.Left)
                {
                    _mousedown = true;
                    _downpoint = mousepos;
                    _aimline.Size = new Vector2f(0, 15);
                }
                else if (EventArgs.Button != Mouse.Button.Left)
                {
                    _mousedown = false;
                }
            }
            base.MouseButtonPressed(Target, EventArgs);
        }
        public override void MouseButtonReleased(RenderTarget Target, MouseButtonEventArgs EventArgs)
        {
            if (EventArgs.Button == Mouse.Button.Left && _mousedown && _canfire)
            {
                if (_aimline.Size.X > 20) if (Fire != null) Fire(_aimline.Size.X / 60f, _aimline.Rotation, _downpoint + Position);
                _mousedown = false;
            }
            base.MouseButtonReleased(Target, EventArgs);
        }
        public override void MouseMoved(RenderTarget Target, MouseMoveEventArgs EventArgs)
        {
            if (_mousedown)
            {
                Vector2f mousepos = Target.MapPixelToCoords(new Vector2i(EventArgs.X, EventArgs.Y));
                mousepos = GetMouseRelativePosition(mousepos);
                _aimline.Size = new Vector2f(Math.Min(TrigHelper.Distance(_downpoint, mousepos), 60), 15);
                _aimline.Rotation = TrigHelper.Angle(_downpoint, mousepos);
                if (_aimline.Size.X > 20) _aimline.FillColor = new Color(0, 130, 0);
                else _aimline.FillColor = new Color(200, 0, 0);
            }
            base.MouseMoved(Target, EventArgs);
        }
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            if (_mousedown)
            {
                _aimline.Position = _downpoint + Position;
                Target.Draw(_aimline, States);
            }
            base.Draw(Target, States);
        }
        public bool Contains(Vector2f Point)
        {
            return _trianglecollision.ContainsPoint(Point.X - Position.X, Point.Y - Position.Y);
        }
        #endregion
    }
}
