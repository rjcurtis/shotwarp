﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.Resources;
using NetEXT.TimeFunctions;

namespace ShotWarp.ScreenManagers.GameScreen_Controls
{
    public class Targets : ControlBase
    {
        #region Variables
        private Texture _targettexture = null;
        private Sprite _targetsprite = null;
        private List<Target> _targets = new List<Target>();
        private Text _countdowntime = null;
        private RectangleShape _overlay = new RectangleShape() { FillColor = new Color(155, 155, 155, 127) };
        #endregion

        #region Properties
        public Target[] TargetList
        {
            get
            {
                return _targets.ToArray();
            }
        }
        #endregion

        #region Constructors
        public Targets(ResourceManager Manager)
        {
            Size = new Vector2f(700, 600);
            Enabled = false;
            _targettexture = Manager.GetTexture(@".\\Game\Target.png");
            _targetsprite = new Sprite(_targettexture);
            _targetsprite.Origin = new Vector2f(44.5f, 74);
            _countdowntime = new Text("", Manager.GetFont(@".\\Fonts\main.ttf")) { Color = Color.Black };
        }
        #endregion

        #region Functions
        public void AddTarget(Target Target)
        {
            _targets.Add(Target);
        }
        public void RemoveTarget(Target Target)
        {
            _targets.Remove(Target);
        }
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            foreach (var target in _targets)
            {
                _targetsprite.Position = target.Position;
                if (target.Type == TargetType.Left)
                {
                    _targetsprite.Scale = new Vector2f(-.5f, .5f);
                    _targetsprite.Rotation = 0;
                }
                else if (target.Type == TargetType.Right)
                {
                    _targetsprite.Scale = new Vector2f(.5f, .5f);
                    _targetsprite.Rotation = 0;
                }
                else if (target.Type == TargetType.Space)
                {
                    _targetsprite.Scale = new Vector2f(.5f, .5f);
                    _targetsprite.Rotation = -90;
                }
                if (target.State == TargetState.Prepared)
                {
                    _targetsprite.Color = new Color(255, 255, 255, 127);
                    Target.Draw(_targetsprite, States);
                    _overlay.Size = new Vector2f(44.5f, 74);
                    _overlay.Origin = new Vector2f(22.25f, 37);
                    _overlay.Position = target.Position;
                    Target.Draw(_overlay, States);
                    _countdowntime.CharacterSize = 40;
                    _countdowntime.Origin = new Vector2f(0, 0);
                    _countdowntime.DisplayedString = Math.Round(target.RemainingTime.Seconds, 0).ToString();
                    _countdowntime.Position = target.Position - new Vector2f(_countdowntime.GetLocalBounds().Width / 2f, _countdowntime.GetLocalBounds().Height / 2f);
                    Target.Draw(_countdowntime);
                }
                else if (target.State == TargetState.Alive)
                {
                    _targetsprite.Color = new Color(255, 255, 255, 255);
                    Target.Draw(_targetsprite, States);
                    if (target.RemainingTime.Ticks > 0)
                    {
                        _overlay.Size = new Vector2f(44.5f, 25);
                        _overlay.Origin = new Vector2f(0, 0);
                        _overlay.Position = target.Position - new Vector2f(22.25f, -35f);
                        Target.Draw(_overlay, States);
                        _countdowntime.CharacterSize = 20;
                        _countdowntime.Origin = new Vector2f(0, 0);
                        _countdowntime.DisplayedString = Math.Round(target.RemainingTime.Seconds, 0).ToString();
                        _countdowntime.Position = _overlay.Position + new Vector2f(22.25f - _countdowntime.GetLocalBounds().Width / 2f, 2);
                        Target.Draw(_countdowntime);
                    }
                }
            }
            base.Draw(Target, States);
        }
        #endregion
    }
}
