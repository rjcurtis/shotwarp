﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.Resources;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.ScreenManagers.GameScreen_Controls
{
    public class GravityDrawing : ControlBase
    {
        #region Variables
        private Dictionary<Gravity, float> _gravitywells = new Dictionary<Gravity, float>();
        private Texture _gravitytexture = null;
        private Sprite _gravitysprite = null;
        #endregion

        #region Constructors
        public GravityDrawing(ResourceManager Manager)
        {
            Size = new Vector2f(700, 600);
            Enabled = false;
            _gravitytexture = Manager.GetTexture(@".\\Game\Gravity.png");
            _gravitysprite = new Sprite(_gravitytexture);
            _gravitysprite.Origin = new Vector2f(150, 150);
        }
        #endregion

        #region Functions
        public void AddGravity(Gravity Gravity)
        {
            _gravitywells.Add(Gravity, RandomGenerator.Random(.1f, .4f));
        }
        public void RemoveGravity(Gravity Gravity)
        {
            _gravitywells.Remove(Gravity);
        }
        public override void Draw(RenderTarget Target, RenderStates States)
        {
            foreach (var gravity in _gravitywells.Keys)
            {
                _gravitysprite.Position = gravity.Position;
                _gravitysprite.Scale = new Vector2f(_gravitywells[gravity], _gravitywells[gravity]);
                Target.Draw(_gravitysprite, States);
            }
            base.Draw(Target, States);
        }
        public override void Update(Time DeltaTime)
        {
            Gravity[] kys = new Gravity[_gravitywells.Keys.Count];
            _gravitywells.Keys.CopyTo(kys, 0);
            foreach (var gravity in kys)
            {
                if (gravity.Type == GravityType.Push)
                {
                    _gravitywells[gravity] += (float)DeltaTime.Seconds;
                    while (_gravitywells[gravity] > .5f) { _gravitywells[gravity] -= .5f; }
                }
                else if (gravity.Type == GravityType.Pull)
                {
                    _gravitywells[gravity] -= (float)DeltaTime.Seconds;
                    while (_gravitywells[gravity] < 0f) { _gravitywells[gravity] += .5f; }
                }
            }
            base.Update(DeltaTime);
        }
        #endregion
    }
}
