﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.ScreenManager;
using Spitfire.Resources;
using ShotWarp.ScreenManagers.GameScreen_Controls;

namespace ShotWarp.ScreenManagers
{
    public class CompletedScreen : ScreenManagerUIBase
    {
        #region Constructors
        public CompletedScreen(Vector2f CurrentScreenSize, ResourceManager CurrentResourceManager)
            : base(CurrentScreenSize, CurrentResourceManager) { }
        #endregion

        #region Functions
        public override void ScreenActivated()
        {
            MusicManager.Pause();
            base.ScreenActivated();
        }
        public override void ScreenDeactivated()
        {
            MusicManager.Play();
            base.ScreenDeactivated();
        }
        protected override void BuildUI()
        {
            AddControl(new TextDisplay(ResourceManager) { FontSize = 40, Color = Color.White, Text = "Level Completed :)", Position = new Vector2f(200, 250) });
            AddControl(new TextDisplay(ResourceManager) { FontSize = 25, Color = Color.White, Text = "(Press 'Enter')", Position = new Vector2f(290, 300) });
        }
        public override void KeyPressed(RenderTarget Target, KeyEventArgs EventArgs)
        {
            if (EventArgs.Code == Keyboard.Key.Return) OnCloseScreen();
            base.KeyPressed(Target, EventArgs);
        }
        #endregion
    }
}
