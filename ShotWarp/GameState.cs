﻿using System;

namespace ShotWarp
{
    public enum GameState
    {
        GameRunning,
        WaitingToFinishLevel,
        WaitingToFailLevel,
        GameFinished
    }
}
