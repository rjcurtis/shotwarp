﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.Application;
using Spitfire.ScreenManager;
using NetEXT.TimeFunctions;
using ShotWarp.ScreenManagers;

namespace ShotWarp
{
    class ProgramEntry
    {
        static void Main(string[] args)
        {
            RenderWindow window = new RenderWindow(new VideoMode(700, 600), "ShotWarp - 2nd SFML Game Jam", Styles.Titlebar | Styles.Close, new ContextSettings(0, 0, 4));
            window.SetVerticalSyncEnabled(true);
            new Controller(window, new MainMenu(new Vector2f(700, 600)), Time.FromSeconds(1f / 45f), CloseMode.Screen).RunLoop();
        }
    }
}
