﻿using System;
using SFML.Window;
using SFML.Graphics;
using Spitfire.UI;
using Spitfire.UI.Controls;
using Spitfire.Resources;

namespace ShotWarp
{
    public static class UIFactory
    {
        public static Button CreateButton(string Text, ResourceManager Manager)
        {
            Button newbutton = new Button(new Vector2f(200, 75), Manager.GetTexture(@".\\UI\button.png"), new IntRect(0, 0, 200, 75), Manager.GetFont(@".\\Fonts\main.ttf"));
            newbutton.Text = Text;
            newbutton.TextColorHandler += Button_TextColorHandler;
            newbutton.TextureRectOffsetHandler += Button_TextureRectOffsetHandler;
            newbutton.ButtonClicked += (button) => { AudioManager.Play(@".\\Sound\button click.ogg"); };
            newbutton.ControlMouseEnter += (control) => { AudioManager.Play(@".\\Sound\button over.ogg"); };
            return newbutton;
        }
        private static Color Button_TextColorHandler(bool Enabled)
        {
            if (Enabled) return Color.Black;
            else return new Color(155, 155, 155);
        }
        private static Vector2i Button_TextureRectOffsetHandler(bool MouseOver, bool Pressed)
        {
            if (Pressed) return new Vector2i(0, 150);
            if (MouseOver) return new Vector2i(0, 75);
            return new Vector2i(0, 0);
        }
    }
}
