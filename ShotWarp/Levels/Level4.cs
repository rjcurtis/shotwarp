﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.Levels
{
    public class Level4 : Level
    {
        #region Variables
        private Time _warpelapsed = Time.Zero;
        private Target _righttarget = null;
        private bool _rightdown = false;
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 2;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 2;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 20;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            OnSetLeftWarpTime(Time.FromSeconds((int)RandomGenerator.Random(2, 6)));
            OnSetRightWarpTime(Time.FromSeconds((int)RandomGenerator.Random(2, 6)));
            OnSetLeftGravity(new Gravity(new Vector2f(0, 0), 10) { Type = GravityType.Pull });
            OnSetRightGravity(new Gravity(new Vector2f(700, 0), 10) { Type = GravityType.Push });
            OnAddTarget(new Target(new Vector2f(70, 250), TargetType.Left, Time.Zero, Time.Zero));
            _righttarget = new Target(new Vector2f(630, 300), TargetType.Right, Time.Zero, Time.Zero);
            OnAddTarget(_righttarget);
        }
        public override void Update(Time DeltaTime)
        {
            _warpelapsed += DeltaTime;
            if (_warpelapsed >= Time.FromSeconds(10))
            {
                _warpelapsed = Time.Zero;
                OnSetLeftWarpTime(Time.FromSeconds((int)RandomGenerator.Random(2, 6)));
                OnSetRightWarpTime(Time.FromSeconds((int)RandomGenerator.Random(2, 6)));
            }
            if (_rightdown) _righttarget.Move(new Vector2f(0, 30 * (float)DeltaTime.Seconds));
            else _righttarget.Move(new Vector2f(0, -30 * (float)DeltaTime.Seconds));
            if (_righttarget.Position.Y < 70) _rightdown = true;
            else if (_righttarget.Position.Y > 530) _rightdown = false;
        }
        #endregion
    }
}
