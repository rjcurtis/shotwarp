﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;

namespace ShotWarp.Levels
{
    public class Level1 : Level
    {
        #region Variables
        private Time _elapsedtime = Time.Zero;
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 2;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 1;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 0;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            OnAddTarget(new Target(new Vector2f(575, 250), TargetType.Right, Time.FromSeconds(15), Time.FromSeconds(5)));
            OnAddTarget(new Target(new Vector2f(125, 250), TargetType.Left, Time.Zero, Time.Zero));
            OnSetLeftWarpTime(Time.FromSeconds(2));
            OnSetRightWarpTime(Time.FromSeconds(2));
        }
        public override void Update(Time DeltaTime)
        {
            _elapsedtime += DeltaTime;
            while (_elapsedtime >= Time.FromSeconds(25))
            {
                _elapsedtime -= Time.FromSeconds(25);
                OnAddTarget(new Target(new Vector2f(575, 250), TargetType.Right, Time.FromSeconds(5), Time.FromSeconds(10)));
            }
        }
        #endregion
    }
}
