﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.Levels
{
    public class Level7 : Level
    {
        #region Variables
        private Time _leftrespawn = Time.Zero;
        private Time _rightrespawn = Time.Zero;
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 4;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 4;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 15;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            OnSetLeftWarpTime(Time.FromSeconds(5));
            OnSetRightWarpTime(Time.FromSeconds(5));
        }
        public override void Update(Time DeltaTime)
        {
            _leftrespawn -= DeltaTime;
            _rightrespawn -= DeltaTime;
            if (_leftrespawn <= Time.Zero)
            {
                _leftrespawn = Time.FromSeconds(RandomGenerator.Random(8, 15));
                if (RandomGenerator.Random(1, 100) <= 49)
                {
                    OnAddTarget(new Target(FindRandomPoint(45, 80, 70, 280) + new Vector2f(0, 0), TargetType.Left, Time.FromSeconds(6), Time.FromSeconds(3)));
                }
                else
                {
                    OnAddTarget(new Target(FindRandomPoint(-80, -45, 70, 280) + new Vector2f(0, 600), TargetType.Left, Time.FromSeconds(6), Time.FromSeconds(3)));
                }
            }
            if (_rightrespawn <= Time.Zero)
            {
                _rightrespawn = Time.FromSeconds(RandomGenerator.Random(8, 15));
                if (RandomGenerator.Random(1, 100) <= 49)
                {
                    OnAddTarget(new Target(FindRandomPoint(100, 135, 70, 280) + new Vector2f(700, 0), TargetType.Right, Time.FromSeconds(6), Time.FromSeconds(3)));
                }
                else
                {
                    OnAddTarget(new Target(FindRandomPoint(-135, -100, 70, 280) + new Vector2f(700, 600), TargetType.Right, Time.FromSeconds(6), Time.FromSeconds(3)));
                }
            }
        }
        private static Vector2f FindRandomPoint(float FirstAngle, float SecondAngle, float MinLength, float MaxLength)
        {
            float angle = RandomGenerator.Random(FirstAngle, SecondAngle);
            float length = RandomGenerator.Random(MinLength, MaxLength);
            return new Vector2f((float)Trigonometry.Cos(angle) * length, (float)Trigonometry.Sin(angle) * length);
        }
        #endregion
    }
}
