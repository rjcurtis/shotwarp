﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;

namespace ShotWarp.Levels
{
    public class Level10 : Level
    {
        #region Variables
        private Time _switchtime = Time.Zero;
        private int _ztoppos = 0;
        private int _zbottompos = 0;
        private Vector2f[] _zpoints = new Vector2f[] { new Vector2f(0, 0), new Vector2f(50, 0), new Vector2f(100, 0), new Vector2f(150, 0), new Vector2f(100, 80), new Vector2f(50, 160), new Vector2f(0, 240), new Vector2f(50, 240), new Vector2f(100, 240), new Vector2f(150, 240) };
        private int _etoppos = 0;
        private int _ebottompos = 0;
        private Vector2f[] _epoints = new Vector2f[] { new Vector2f(150, 0), new Vector2f(100, 0), new Vector2f(50, 0), new Vector2f(0, 0), new Vector2f(0, 80), new Vector2f(50, 120), new Vector2f(0, 160), new Vector2f(0, 240), new Vector2f(50, 240), new Vector2f(100, 240), new Vector2f(150, 240) };
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 20;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 20;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 45;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            _ztoppos = 0;
            _zbottompos = _zpoints.Length - 1;
            _etoppos = 0;
            _ebottompos = _epoints.Length - 1;
            OnSetLeftWarpTime(Time.FromSeconds(6));
            OnSetRightWarpTime(Time.FromSeconds(6));
            OnSetLeftGravity(new Gravity(new Vector2f(40, 300), 45));
            OnSetRightGravity(new Gravity(new Vector2f(660, 300), 45));
        }
        public override void Update(Time DeltaTime)
        {
            _switchtime -= DeltaTime;
            if (_switchtime <= Time.Zero)
            {
                _switchtime = Time.FromSeconds(5);

                OnAddTarget(new Target(_zpoints[_ztoppos] + new Vector2f(50, 143), TargetType.Left, Time.FromSeconds(5), Time.FromSeconds(5)));
                if (_zbottompos != _ztoppos) OnAddTarget(new Target(_zpoints[_zbottompos] + new Vector2f(50, 143), TargetType.Left, Time.FromSeconds(5), Time.FromSeconds(5)));
                _ztoppos += 1;
                _zbottompos -= 1;
                if ((float)_ztoppos >= (float)_zpoints.Length / 2f)
                {
                    _ztoppos = 0;
                    _zbottompos = _zpoints.Length - 1;
                }

                OnAddTarget(new Target(_epoints[_etoppos] + new Vector2f(505.5f, 143), TargetType.Right, Time.FromSeconds(5), Time.FromSeconds(5)));
                if (_ebottompos != _etoppos) OnAddTarget(new Target(_epoints[_ebottompos] + new Vector2f(505.5f, 143), TargetType.Right, Time.FromSeconds(5), Time.FromSeconds(5)));
                _etoppos += 1;
                _ebottompos -= 1;
                if ((float)_etoppos >= (float)_epoints.Length / 2f)
                {
                    _etoppos = 0;
                    _ebottompos = _epoints.Length - 1;
                }
            }
        }
        #endregion
    }
}
