﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;

namespace ShotWarp.Levels
{
    public class Level5 : Level
    {
        #region Variables
        private Target _lefttarget = null;
        private Target _righttarget = null;
        private bool _leftdown = true;
        private bool _rightdown = false;
        #endregion

        #region Properties
        public override int RequiredLeftTargets
        {
            get
            {
                return 4;
            }
        }
        public override int RequiredRightTargets
        {
            get
            {
                return 4;
            }
        }
        public override int RequiredSpaceTargets
        {
            get
            {
                return 0;
            }
        }
        public override int TotalAmmo
        {
            get
            {
                return 15;
            }
        }
        #endregion

        #region Functions
        public override void Initialize()
        {
            OnSetLeftWarpTime(Time.FromSeconds(2));
            OnSetRightWarpTime(Time.FromSeconds(3));
            _lefttarget = new Target(new Vector2f(70, 70), TargetType.Left, Time.Zero, Time.Zero);
            OnAddTarget(_lefttarget);
            _righttarget = new Target(new Vector2f(630, 300), TargetType.Right, Time.Zero, Time.Zero);
            OnAddTarget(_righttarget);
        }
        public override void Update(Time DeltaTime)
        {
            if (_leftdown) _lefttarget.Move(new Vector2f(0, 20 * (float)DeltaTime.Seconds));
            else _lefttarget.Move(new Vector2f(0, -20 * (float)DeltaTime.Seconds));
            if (_rightdown) _righttarget.Move(new Vector2f(0, 20 * (float)DeltaTime.Seconds));
            else _righttarget.Move(new Vector2f(0, -20 * (float)DeltaTime.Seconds));
            if (_lefttarget.Position.Y < 70) _leftdown = true;
            else if (_lefttarget.Position.Y > 530) _leftdown = false;
            if (_righttarget.Position.Y < 70) _rightdown = true;
            else if (_righttarget.Position.Y > 530) _rightdown = false;
        }
        #endregion
    }
}
