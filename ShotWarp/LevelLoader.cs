﻿using System;

namespace ShotWarp
{
    public class LevelLoader
    {
        #region Variables
        private int _level = 1;
        private int _maxlevel = 10;
        #endregion

        #region Properties
        public bool IsFinalLevel
        {
            get
            {
                return _level == _maxlevel;
            }
        }
        #endregion

        #region Functions
        public Level LoadNextLevel()
        {
            _level += 1;
            return LoadLevel();
        }
        public Level LoadCurrentLevel()
        {
            return LoadLevel();
        }
        private Level LoadLevel()
        {
            switch (_level)
            {
                case 1: return new Levels.Level1();
                case 2: return new Levels.Level2();
                case 3: return new Levels.Level3();
                case 4: return new Levels.Level4();
                case 5: return new Levels.Level5();
                case 6: return new Levels.Level6();
                case 7: return new Levels.Level7();
                case 8: return new Levels.Level8();
                case 9: return new Levels.Level9();
                case 10: return new Levels.Level10();
                default: return new Levels.Level1();
            }
        }
        #endregion
    }
}
