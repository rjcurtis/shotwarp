﻿using System;
using NetEXT.TimeFunctions;

namespace ShotWarp
{
    public abstract class Level
    {
        public abstract int RequiredLeftTargets { get; }
        public abstract int RequiredRightTargets { get; }
        public abstract int RequiredSpaceTargets { get; }
        public abstract int TotalAmmo { get; }
        public abstract void Update(Time DeltaTime);
        public abstract void Initialize();
        public virtual void TargetHit(Target Target) { }
        public event Action<Gravity> SetLeftGravity;
        public event Action<Gravity> SetRightGravity;
        public event Action<Gravity> SetSpaceGravity;
        public event Action<GravityState> SetGravityState;
        public event Action<Target> AddTarget;
        public event Action<Time> SetLeftWarpTime;
        public event Action<Time> SetRightWarpTime;
        public event Action<Time> SetSpaceWarpTime;
        protected void OnSetLeftGravity(Gravity Gravity) { if (SetLeftGravity != null) SetLeftGravity(Gravity); }
        protected void OnSetRightGravity(Gravity Gravity) { if (SetRightGravity != null) SetRightGravity(Gravity); }
        protected void OnSetSpaceGravity(Gravity Gravity) { if (SetSpaceGravity != null) SetSpaceGravity(Gravity); }
        protected void OnSetGravityState(GravityState GravityState) { if (SetGravityState != null) SetGravityState(GravityState); }
        protected void OnAddTarget(Target Target) { if (AddTarget != null) AddTarget(Target); }
        protected void OnSetLeftWarpTime(Time Time) { if (SetLeftWarpTime != null) SetLeftWarpTime(Time); }
        protected void OnSetRightWarpTime(Time Time) { if (SetRightWarpTime != null) SetRightWarpTime(Time); }
        protected void OnSetSpaceWarpTime(Time Time) { if (SetSpaceWarpTime != null) SetSpaceWarpTime(Time); }
    }
}
